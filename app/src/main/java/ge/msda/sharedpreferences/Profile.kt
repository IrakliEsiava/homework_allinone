package ge.msda.sharedpreferences

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_profile.*

class Profile : AppCompatActivity() {

    private lateinit var prefs : SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        prefs = getSharedPreferences("USER", Context.MODE_PRIVATE)

        val email = prefs.getString("EMAIL", "NOT FOUND");
        val pass = prefs.getString("PASSWORD", "")

        txtEmail.setText(email)
        txtPassword.setText(pass)

        savebtn.setOnClickListener {
            if(txtIMGURL.text.toString().isNotEmpty())
            {
                Glide.with(this).load(txtIMGURL.text.toString()).into(profimig);
            }
        }
    }
}
