package ge.msda.sharedpreferences

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var prefs : SharedPreferences;
    private lateinit var editPrefs : SharedPreferences.Editor; ;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        prefs = getSharedPreferences("USER", Context.MODE_PRIVATE)
        editPrefs = prefs.edit()

        Glide.with(this).load("https://img3.goodfon.com/wallpaper/nbig/0/9c/google-google-play-google.jpg")
            .into(bgImage)


        loginbtn.setOnClickListener { CheckUser() }


    }

    private fun CheckUser()
    {
        if(userEmail.text.toString().isNotEmpty() && userPassword.text.toString().isNotEmpty())
        {
            editPrefs.putString("EMAIL", userEmail.text.toString());
            editPrefs.putString("PASSWORD", userPassword.text.toString());
            editPrefs.apply()

            Toast.makeText(this, "Login success.", Toast.LENGTH_LONG).show()
            startActivity(Intent(this, Profile::class.java))
        } else {
            Toast.makeText(this, "All fields are required.", Toast.LENGTH_LONG).show()
        }
    }
}